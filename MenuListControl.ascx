﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuListControl.ascx.cs" Inherits="MenuListControl" %>
<div class="mainmenu">
           <div class="onemainmenu" id=0>
				<div class="menuTexthome"><asp:HyperLink ID="hl_home" runat="server" NavigateUrl="~/Default.aspx" Target="_self">Home</asp:HyperLink></div>
			</div>
            <div class="onemainmenu" id=1>
				<div class="tri1"></div>
				<div class="menuText">自动取号</div>
			</div>
            <div class="submenu">
				<div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">报价单取号</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">报价单取号查询表</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">合同取号</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">潜在客户信息</div>
				</div>
             </div>	

            <div class="onemainmenu" id=2>
				<div class="tri1"></div>
				<div class="menuText">销售信息</div>
			</div>
            <div class="submenu">
				<div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">
                        <asp:HyperLink ID="hl_contractInfoList" runat="server" NavigateUrl="~/WebPage/proGetContractList2013.aspx">合同一览表</asp:HyperLink></div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText"><asp:HyperLink ID="hl_contractContractUnion" runat="server" NavigateUrl="~/WebPage/proGetContractUnion2013.aspx">合同合并表</asp:HyperLink></div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">按部门和产品大类统计</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">按部和可产品统计</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">部门费用报表</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">Fisher Cost报表</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">销售部门FOR</div>
				</div>
                <div class="onesubmenu">
					<div class="report"></div>
					<div class="menuText">应收应付汇总表</div>
				</div>
             </div>	