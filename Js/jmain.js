
function initsize() {
    var wheight = $(window).height(); // 浏览器的高度
    var wwidth = $(window).width();   //浏览器的高度
    $(".left").height(wheight);
    $(".mainmenu").height(wheight - 87);
    $(".reportview").height(wheight - 71);
    $(".reportview").width(wwidth - 260);
    $(".right-content").height(wheight - 120);
    $(".left-top").width(260);
    $(".right").height(wheight);
 
}

function rightsize1() {
    var wheight = $(window).height(); // 浏览器的高度
    var wwidth = $(window).width();   //浏览器的高度
    $(".reportview").width(wwidth - 15);
}
function rightsize2() {
    var wheight = $(window).height(); // 浏览器的高度
    var wwidth = $(window).width();   //浏览器的高度
    $(".reportview").width(wwidth - 260);
}
$(function () {
    var mainindex = -1; // 主菜单的索引值
    var subindex = -1; // 子菜单的索引值

    //调整左侧菜单的高度
    initsize();


    // 鼠标滑过主菜单事件
    $(".onemainmenu").hover(function () {
        var ind = $(".onemainmenu").index(this); // 鼠标滑过主菜单，主菜单的位置
        if (ind != mainindex) {
            $(this).addClass("mainhover");
        }

    }, function () {
        $(this).removeClass("mainhover");

    });

    // 鼠标滑过子菜单事件
    $(".onesubmenu").hover(function () {
        if ($(".onesubmenu").index(this) != subindex) {
            $(this).addClass("subhover");
        }
    }, function () {
        if ($(".onesubmenu").index(this) != subindex != subindex) {
            $(this).removeClass("subhover");
            ;
        }
    });

    $(".onesubmenu").click(function () {
         
        $(".mainfocus").removeClass("mainfocus"); // 消除所有父菜单的背景
        $(".subfocus").removeClass("subfocus"); // 消除所有父菜单的背景
        mainindex = -1;
        $(this).addClass("subfocus");
    });
    // 鼠标单击主菜单事件，用toggle方法
    $(".onemainmenu").toggle(function () {
        $(".mainfocus").removeClass("mainfocus"); // 消除所有父菜单的背景
        $(this).removeClass("mainhover"); // 去除鼠标划过改变的背景
        $(this).addClass("mainfocus"); // 将当前菜单背景改变
        $(this).children(".tri1").addClass("tri1focus"); // 变换菜单左侧的图片
        $(this).next().addClass("subshow"); // 显示子菜单
        mainindex = $(".onemainmenu").index(this); // 记录索引值
        $(".onemainmenu .submenu .onesubmenu").removeClass("subfocus"); // 单击主菜单将所有子菜单取消背景
        subindex = -1; // 子菜单索引复位
    }, function () {
        $(".subfocus").removeClass("subfocus");
        $(".mainfocus").removeClass("mainfocus"); // 消除所有父菜单的背景
        $(this).addClass("mainfocus");
        $(this).children(".tri1").removeClass("tri1focus"); // 变换菜单左侧的图片
        $(".onemainmenu .submenu .onesubmenu").removeClass("subfocus"); // 单击主菜单将所有子菜单取消背景
        $(this).next().removeClass("subshow"); // 隐藏子菜单
        subindex = -1; // 子菜单索引复位
    });

    //左侧菜单缩进操作
    $(".left-slidbar").toggle(function () {
        $(".mainmenu").animate({ width: "0px" }, 0);

        $(".left-top").animate({ width: "0px" }, 0);
        $(this).animate({ width: "18px" }, 0);
        $(".left-slidbar").children().remove();
        $(".left-slidbar").append(retHtml); // 增加标签
        function retHtml() {
            var $tab = $('.newhidden .show');
            $tab = $tab.clone();
            return $tab;
        }
        $(".left").addClass("leftcolor");
        $(".right").addClass("rightlarge");
        rightsize1();
    },
    function () {
        $(".mainmenu").animate({ width: "260px" }, 0);
        $(".left-top").animate({ width: "260px" }, 0);
        $(this).animate({ width: "260px" }, 0);
        $(".left-slidbar").children().remove();
        $(".left-slidbar").append(retHtml); // 增加标签
        function retHtml() {
            var $tab = $('.oldhidden .show');
            $tab = $tab.clone();
            return $tab;
        }
        $(".left").removeClass("leftcolor");
        $(".right").removeClass("rightlarge");
        rightsize2();
    });

    $(".left").click(function () {

        $(".mainmenu").animate({ width: "260px" }, 0);
        $(".left-top").animate({ width: "260px" }, 0);
        $(".left-slidbar").animate({ width: "260px" }, 0);
        $(".left-slidbar").children().remove();
        $(".left-slidbar").append(retHtml); // 增加标签
        function retHtml() {
            var $tab = $('.oldhidden .show');
            $tab = $tab.clone();
            return $tab;
        }
        $(".left").removeClass("leftcolor");
        $(".right").removeClass("rightlarge");
        rightsize2();
    });

    $(window).resize(function () {

        initsize();
    });


});