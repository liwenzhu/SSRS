﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="proGetContractList2013.aspx.cs" Inherits="_Default" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
     <script src="../Js/jmain.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="welcome">welcome demo</div>
      <div class="search">
           <div class="toolbar">
              <!--条件一-->
              <div class="condition"><div class="text">年度:</div>
                  <div class="tool">
                      <asp:DropDownList ID="year" runat="server" AppendDataBoundItems="True">
                      <asp:ListItem>2008</asp:ListItem>
                      <asp:ListItem>2009</asp:ListItem>
                      <asp:ListItem>2010</asp:ListItem>
                      <asp:ListItem>2011</asp:ListItem>
                      <asp:ListItem>2012</asp:ListItem>
                      <asp:ListItem>2013</asp:ListItem>
                      <asp:ListItem>2014</asp:ListItem>
                      <asp:ListItem>2015</asp:ListItem>
                      </asp:DropDownList>
                  </div>
              </div>
              <!--条件一结束-->

               <!--条件二-->
               <div class="condition"><div class="text">Form date:</div>
                  <div class="tool">
                     <asp:DropDownList ID="Formdate" runat="server">
                         <asp:ListItem>2008</asp:ListItem>
                         <asp:ListItem>2009</asp:ListItem>
                         <asp:ListItem>2010</asp:ListItem>
                         <asp:ListItem>2011</asp:ListItem>
                         <asp:ListItem>2012</asp:ListItem>
                         <asp:ListItem>2013</asp:ListItem>
                         <asp:ListItem>2014</asp:ListItem>
                         <asp:ListItem>2015</asp:ListItem>
                     </asp:DropDownList>
                  </div>     
              </div>
             <!--条件二结束-->

            <!--条件三-->
               <div class="condition"><div class="text">To date:</div>
                  <div class="tool">

                      <asp:DropDownList ID="Todate" runat="server">
                          <asp:ListItem>2008</asp:ListItem>
                          <asp:ListItem>2009</asp:ListItem>
                          <asp:ListItem>2010</asp:ListItem>
                          <asp:ListItem>2011</asp:ListItem>
                          <asp:ListItem>2012</asp:ListItem>
                          <asp:ListItem>2013</asp:ListItem>
                          <asp:ListItem>2014</asp:ListItem>
                          <asp:ListItem>2015</asp:ListItem>
                      </asp:DropDownList>

                  </div>     
              </div>
             <!--条件三结束-->

               <!--条件四-->
               <div class="condition"><div class="text">Dept:</div>
                  <div class="tool">
                    
                      <asp:DropDownList ID="Dept" runat="server">
                          <asp:ListItem>Key Account Sales</asp:ListItem>
                          <asp:ListItem>Key Account</asp:ListItem>
                          <asp:ListItem>MNC</asp:ListItem>
                          <asp:ListItem>Jiang Su Non-Powe</asp:ListItem>
                          <asp:ListItem>Shanghai Non-Power</asp:ListItem>
                          <asp:ListItem>Non-Power</asp:ListItem>
                          <asp:ListItem>Denox/Desox</asp:ListItem>
                      </asp:DropDownList> 
                  </div>     
              </div>
             <!--条件四结束-->
          </div>

          <div class="toolbar">
              <!--条件一-->
              <div class="condition"><div class="text">合同号:</div>
                  <div class="tool">
                      <asp:TextBox ID="ContractNo" runat="server" Width="50px"></asp:TextBox>
                  </div>
              </div>
              <!--条件一结束-->

              <!--条件二-->
               <div class="condition"><div class="text">Sales Engineer:</div>
                  <div class="tool">
                     <asp:DropDownList ID="SalesEngineer" runat="server">
                         <asp:ListItem>2008</asp:ListItem>
                         <asp:ListItem>2009</asp:ListItem>
                         <asp:ListItem>2010</asp:ListItem>
                         <asp:ListItem>2011</asp:ListItem>
                         <asp:ListItem>2012</asp:ListItem>
                         <asp:ListItem>2013</asp:ListItem>
                         <asp:ListItem>2014</asp:ListItem>
                         <asp:ListItem>2015</asp:ListItem>
                     </asp:DropDownList>
                  </div>     
              </div>
             <!--条件二结束-->

               <!--条件三-->
               <div class="condition"><div class="text"></div>
                  <div class="tool">
                  </div>     
              </div>
             <!--条件三结束-->

               <!--条件四-->
               <div class="condition"><div class="text">AE Engineer:</div>
                  <div class="tool">
                    
                      <asp:DropDownList ID="AEEngineer" runat="server">
                          <asp:ListItem>Key Account Sales</asp:ListItem>
                          <asp:ListItem>Key Account</asp:ListItem>
                          <asp:ListItem>MNC</asp:ListItem>
                          <asp:ListItem>Jiang Su Non-Powe</asp:ListItem>
                          <asp:ListItem>Shanghai Non-Power</asp:ListItem>
                          <asp:ListItem>Non-Power</asp:ListItem>
                          <asp:ListItem>Denox/Desox</asp:ListItem>
                      </asp:DropDownList> 
                  </div>     
              </div>
             <!--条件四结束-->


          </div>

          <div class="toolbar">

              <!--条件二-->
               <div class="condition"><div class="text">Customer Contract No.:</div>
                  <div class="tool">
                      <asp:TextBox ID="CustomerContractNo" runat="server" Width="50px"></asp:TextBox>
                  </div>     
              </div>
             <!--条件二结束-->

               <!--条件四-->
               <div class="condition"><div class="text">Customer Name:</div>
                  <div class="tool">
                    
                      <asp:DropDownList ID="CustomerName" runat="server">
                          <asp:ListItem>Key Account Sales</asp:ListItem>
                          <asp:ListItem>Key Account</asp:ListItem>
                          <asp:ListItem>MNC</asp:ListItem>
                          <asp:ListItem>Jiang Su Non-Powe</asp:ListItem>
                          <asp:ListItem>Shanghai Non-Power</asp:ListItem>
                          <asp:ListItem>Non-Power</asp:ListItem>
                          <asp:ListItem>Denox/Desox</asp:ListItem>
                      </asp:DropDownList> 
                  </div>     
              </div>
             <!--条件四结束-->

              <div class="condition">
                
                  <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="搜索" Width="76px" />
                
              </div>


          </div>

      </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div class="content">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="reportview" EnableTheming="False" Font-Names="Verdana" Font-Size="8pt" Height="100%" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" OnReportRefresh="ReportViewer1_ReportRefresh">
            <LocalReport ReportPath="Report\proGetContractList2013.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>        
       
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="DataSetTableAdapters."></asp:ObjectDataSource>
       
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="DataSet+DataTable1DataTable" SelectMethod="Copy"></asp:ObjectDataSource>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </div>
</asp:Content>

