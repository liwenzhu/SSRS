﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    SqlDataBase db = new SqlDataBase();
    ReportDataSource rds;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            db = new SqlDataBase();
            DbParameter[] dbpc = {
                new SqlParameter("@year","2011"),
                new SqlParameter("@startdate",DBNull.Value),
                new SqlParameter("@enddate",DBNull.Value),
                new SqlParameter("@ContractNo",DBNull.Value),
                new SqlParameter("@Dept",DBNull.Value),
                new SqlParameter("@AEName",DBNull.Value),
                new SqlParameter("@salesname",DBNull.Value),
                new SqlParameter("@CustContractNo",DBNull.Value),
                new SqlParameter("@CustomerName",DBNull.Value)

            };
            DataSet ds = db.RunProcedureGetDataSet("proGetContractList2013", dbpc);
            DataTable dt = ds.Tables["Table"];
            rds = new ReportDataSource("DataSet1", dt);
            this.ReportViewer1.LocalReport.DataSources.Clear();

            this.ReportViewer1.LocalReport.DataSources.Add(rds);
            this.ReportViewer1.LocalReport.Refresh();
        }
    }
    protected void ReportViewer1_ReportRefresh(object sender, System.ComponentModel.CancelEventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DbParameter[] dbpc = {
                new SqlParameter("@year",DBNull.Value),
                new SqlParameter("@startdate",DBNull.Value),
                new SqlParameter("@enddate",DBNull.Value),
                new SqlParameter("@ContractNo",this.ContractNo.Text),
                new SqlParameter("@Dept",DBNull.Value),
                new SqlParameter("@AEName",DBNull.Value),
                new SqlParameter("@salesname",DBNull.Value),
                new SqlParameter("@CustContractNo",DBNull.Value),//1501291
                new SqlParameter("@CustomerName",DBNull.Value)

            };

        DataSet ds = db.RunProcedureGetDataSet("proGetContractList2013", dbpc);
        DataTable dt = ds.Tables["Table"];
        rds = new ReportDataSource("DataSet1", dt);
        this.ReportViewer1.LocalReport.DataSources.Clear();
        this.ReportViewer1.LocalReport.DataSources.Add(rds);
        this.ReportViewer1.LocalReport.Refresh();
    }
}